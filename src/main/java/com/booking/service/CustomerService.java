package com.booking.service;
import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Person;

public class CustomerService {
    public static List<Customer> getAllCustomers(List<Person> personList){
        return personList.stream()
                    .filter(person -> person instanceof Customer)
                    .map(person -> (Customer) person)
                    .toList();
    }

    public static Customer getCustomerById(String customerId, List<Person> personList){
        return CustomerService.getAllCustomers(personList).stream()
                        .filter(customer -> customer.getId().equalsIgnoreCase(customerId))
                        .findFirst()
                        .orElse(null);
        
    }

    public static double getPercentageMember(Customer customer){
        String memberType = customer.getMember().getMembershipName();

        double percentageMember = (memberType.equalsIgnoreCase("none"))? 0 : (memberType.equalsIgnoreCase("silver"))? 0.05 : (memberType.equalsIgnoreCase("gold"))? 0.1 : 0;

        return percentageMember;
    }

    
}
