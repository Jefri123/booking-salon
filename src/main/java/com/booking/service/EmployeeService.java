package com.booking.service;

import java.util.List;

import com.booking.models.Employee;
import com.booking.models.Person;

public class EmployeeService {
    public static List<Employee> getAllEmployee(List<Person> personList){
        return personList.stream()
                    .filter(person -> person instanceof Employee)
                    .map(person -> (Employee) person)
                    .toList();
    }

    public static Employee getEmployeeById(String employeeId, List<Person> personList){
        return EmployeeService.getAllEmployee(personList).stream()
                                .filter(employee -> employee.getId().equalsIgnoreCase(employeeId))
                                .findFirst()
                                .orElse(null);
    }
}