package com.booking.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static List<Reservation> reservationList = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/cancel reservation", "Exit"};
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "Tampilkan History Reservation + Total Keuntungan", "Back to main menu"};
    
        int optionMainMenu;
        int optionSubMenu;

		boolean backToMainMenu = false;
        boolean backToSubMenu = false;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            System.out.print("\nMasukan Pilihan Menu : ");
            optionMainMenu = Integer.valueOf(input.nextLine());
            switch (optionMainMenu) {
                case 1:
                backToSubMenu = false;
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        System.out.print("\nMasukan Pilihan Menu : ");
                        optionSubMenu = Integer.valueOf(input.nextLine());
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                // panggil fitur tampilkan recent reservation
                                PrintService.showHistoryReservation(reservationList, false);
                                input.nextLine();
                                break;
                            case 2:
                                // panggil fitur tampilkan semua customer
                                PrintService.showAllCustomer(CustomerService.getAllCustomers(personList));
                                input.nextLine();
                                break;
                            case 3:
                                // panggil fitur tampilkan semua employee
                                PrintService.showAllEmployee(EmployeeService.getAllEmployee(personList));
                                input.nextLine();
                                break;
                            case 4:
                                PrintService.showHistoryReservation(reservationList, true);
                                input.nextLine();
                                // panggil fitur tampilkan history reservation + total keuntungan
                                break;
                            case 0:
                                backToSubMenu = true;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                if ((EmployeeService.getAllEmployee(personList)).size() > 0){
                    PrintService.showAllCustomer(CustomerService.getAllCustomers(personList));
                    String customerId = ValidationService.validateCustomerId(personList);
                    
                    PrintService.showAllEmployee(EmployeeService.getAllEmployee(personList));
                    String employeeId = ValidationService.validateEmployeeId(personList);
                
                    PrintService.showAllService(serviceList);
                    List<Service> services = ValidationService.valideServiceId(serviceList);

                    Reservation reservation = ReservationService.createReservation(customerId, employeeId, services, personList);
                    
                    System.out.println("Booking Berhasil !");
                    System.out.println("Total Biaya Booking : " + (new DecimalFormat("Rp #,##0.00")).format(reservation.getReservationPrice()));

                    reservationList.add(reservation);
                    personList.remove(EmployeeService.getEmployeeById(employeeId, personList));

                    } else {
                        System.out.println("Tidak ada employee yang tersedia");
                    }
                   
                    input.nextLine();
                    break;
                case 3:
                    // panggil fitur mengubah workstage menjadi finish/cancel
                    PrintService.showReservationToCustomer(reservationList);

                    if (ReservationService.getCountReservationInProcess(reservationList) > 0){      
                        String reservationId = ValidationService.validateReservationId(reservationList);

                        String reservationActions = ValidationService.validateInput("Selesaikan Reservasi : ", "^(Finish|Cancel|finish|cancel)$", "Input hanya boleh Finish / Cancel !");
                        
                        ReservationService.editReservationWorkstage(reservationList, reservationId, reservationActions);
                        System.out.println("Reservasi dengan ID " + reservationId + " sudah " + reservationActions);
                        
                        personList.add(ReservationService.getEmployeeByReservationId(reservationList, reservationId));
                    }
                    input.nextLine();
                    break;
                case 0:
                    backToMainMenu = true;
                    break;
            }
        } while (!backToMainMenu);
		
	}

    
}
