package com.booking.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        System. out. print("\033[H\033[2J");
        System. out. flush();
        int num = 1;
        System.out.println(" ".repeat(10) + title + "\n");
        // System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList){
        return serviceList
                .stream()
                .map(Service::getServiceName)
                .collect(Collectors.joining(", "));
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public static void showAllCustomer(List<Customer> customerList){
        int num = 1;
        System.out.printf("\n| %-4s | %-7s | %-15s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Alamat", "Membership", "Uang");
        System.out.println("+========================================================================================+");
        for (Customer customer : customerList) {
                System.out.printf("| %-4s | %-7s | %-15s | %-15s | %-15s | %-15s |\n",
                num, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), (new DecimalFormat("Rp #,##0.00")).format(customer.getWallet()));
                num++;
            }
        }

    public static void showAllEmployee(List<Employee> allEmployee){
        int num = 1;
        System.out.printf("\n| %-4s | %-7s | %-15s | %-15s | %-15s\n",
                "No.", "ID", "Nama Employee", "Alamat", "Pengalaman");
        System.out.println("+========================================================================================+");
        for (Employee employee : allEmployee) {
                System.out.printf("| %-4s | %-7s | %-15s | %-15s | %-15s\n",
                num, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
                num++;
            }
        }

    public static void showAllService(List<Service> allService){
        int num = 1;
        System.out.printf("\n| %-4s | %-7s | %-25s | %-15s\n",
                "No.", "ID", "Service", "Harga");
        System.out.println("+=============================================================================================+");
        for (Service service : allService) {
                System.out.printf("| %-4s | %-7s | %-25s | %-15s\n",
                num, service.getServiceId(), service.getServiceName(), (new DecimalFormat("Rp #,##0.00")).format(service.getPrice()));
                num++;
            }
        }

    public static void showHistoryReservation(List<Reservation> allReservation, boolean totalPrice){
        int num = 1;
        System.out.printf("\n| %-4s | %-5s | %-12s | %-50s | %-15s | %-15s\n",
        "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println("+=================================================================================================================+");
        for (Reservation reservation : allReservation) {
            if (totalPrice == false){
                if (reservation.getWorkstage().equalsIgnoreCase("in process")){
                    System.out.printf("| %-4s | %-5s | %-12s | %-50s | %-15s | %-15s\n",
                    num, reservation.getReservationId(), reservation.getCustomer().getName(), truncateText(PrintService.printServices(reservation.getServices()), 50), (new DecimalFormat("Rp #,##0.00")).format(reservation.getReservationPrice()), reservation.getWorkstage());
                    num++;
                }
            } else{
                System.out.printf("| %-4s | %-5s | %-12s | %-50s | %-15s | %-15s\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), truncateText(PrintService.printServices(reservation.getServices()), 50), (new DecimalFormat("Rp #,##0.00")).format(reservation.getReservationPrice()), reservation.getWorkstage());
                num++;
            }
        }

        if (totalPrice){
            System.out.println("+=================================================================================================================+");
            System.out.print("Total Keuntungan : ");
            System.out.print(" ".repeat(65) +(new DecimalFormat("Rp #,##0.00")).format(ReservationService.getTotalPriceReservation(allReservation)));
        }
    }

    public static void showReservationToCustomer(List<Reservation> allReservation){
        int num = 1;
        System.out.printf("\n| %-4s | %-5s | %-12s | %-50s | %-15s\n",
        "No.", "ID", "Nama Customer", "Service", "Total Biaya");
        System.out.println("+==================================================================================================+");
        for (Reservation reservation : allReservation) {
            if (reservation.getWorkstage().equalsIgnoreCase("in process")){
                System.out.printf("| %-4s | %-5s | %-12s | %-50s | %-15s\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), truncateText(PrintService.printServices(reservation.getServices()), 50), (new DecimalFormat("Rp #,##0.00")).format(reservation.getReservationPrice()));
                num++;
            } 
        }
    }

    public static String truncateText(String text, int maxLength) {
        if (text.length() <= maxLength) {
            return text;
        }
        return text.substring(0, maxLength - 3) + "...";
    }
}
