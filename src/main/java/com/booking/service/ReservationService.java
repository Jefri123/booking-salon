package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    private static Integer reservationCount = 0;

    public static String generateId(){
        reservationCount++;
        return String.format("Rsv" +"-%02d", reservationCount);
    }

    public static Reservation createReservation(String customerId, String employeeId, List<Service> services, List<Person> allPerson){
        Reservation reservation = Reservation.builder()
                                    .customer(CustomerService.getCustomerById(customerId, allPerson))
                                    .employee(EmployeeService.getEmployeeById(employeeId, allPerson))
                                    .services(services)
                                    .workstage("In Process")
                                    .reservationId(generateId())
                                    .build();
                                    
        reservation.setReservationPrice(reservation.calculateReservationPrice());
        return reservation;
    }

    public static double getTotalPriceReservation(List<Reservation> allReservation){
        return allReservation.stream()
                .filter(reservation -> reservation.getWorkstage().equalsIgnoreCase("finish"))
                .mapToDouble(Reservation::getReservationPrice)
                .sum();
    }

    public static void editReservationWorkstage(List<Reservation> allReservation, String reservationId, String workstage){
        allReservation.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationId))
                .findFirst()
                .ifPresent(reservation -> reservation.setWorkstage(workstage));
    }

    public static int getCountReservationInProcess(List<Reservation> allReservations){
        return (int) allReservations.stream()
                    .filter(reservation -> reservation.getWorkstage().equalsIgnoreCase("in process"))
                    .count();
    }

    public static Reservation getReservationByReservationId(List<Reservation> allReservation, String reservationId){
        return allReservation.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationId))
                .findFirst()
                .orElse(null);
    }

    public static Customer getCustomerByReservationId(List<Reservation> allReservation, String reservationId){
        return allReservation.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationId))
                .map(Reservation::getCustomer)
                .findFirst()
                .orElse(null);
    }

    public static Employee getEmployeeByReservationId(List<Reservation> allReservation, String reservationId){
        return allReservation.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationId))
                .map(Reservation::getEmployee)
                .findFirst()
                .orElse(null);
    }
    
}
