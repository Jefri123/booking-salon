package com.booking.service;

import java.util.List;

import com.booking.models.Service;
import com.booking.repositories.ServiceRepository;

public class SalonService {

    public static double getServicePriceByName(List<Service> allService){ 
        return allService.stream()
                .mapToDouble(Service::getPrice)
                .sum();
    }

    public static Service getServiceById(String serviceId, List<Service> serviceList){
        return ServiceRepository.getAllService().stream()
                                .filter(service -> service.getServiceId().equalsIgnoreCase(serviceId))
                                .findFirst()
                                .orElse(null);
    }
}
