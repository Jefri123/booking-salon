package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ValidationService {
    private static Scanner input = new Scanner(System.in);
    // Buatlah function sesuai dengan kebutuhan
    public static String validateInput(String userDisplay, String regexInp, String errorMessage){
        while (true){
            System.out.print(userDisplay);
            String inp = input.nextLine();

            Pattern pattern = Pattern.compile(regexInp);
            Matcher matcher = pattern.matcher(inp);

            if (matcher.matches()) {
                return inp;
            }
            System.out.println(errorMessage);
        }
    }

    public static String validateCustomerId(List<Person> allPerson){
        while (true) {
            System.out.print("\nSilahkan Masukan Customer Id : ");
            String customerId = input.nextLine();
        
            Customer customerTemp = CustomerService.getCustomerById(customerId, allPerson);
            
            if (customerTemp != null){
                return customerId;
            }

            System.out.println("Customer yang dicari tidak tersedia");
        }
    }

    public static String validateEmployeeId(List<Person> allPerson){
        while (true) {
            System.out.print("\nSilahkan Masukan Employee Id : ");
            String employeeId = input.nextLine();

            Employee employeeTemp = EmployeeService.getEmployeeById(employeeId, allPerson);

            if (employeeTemp != null){
                return employeeId;
            }

            System.out.println("Employee yang dicari tidak tersedia");
        }
    }

    public static boolean isServiceAlreadyInputed(List<Service> allServiceInput, Service serviceInp){
        if (allServiceInput.size() > 0){
            for (Service service: allServiceInput){
                if (service.getServiceId().equalsIgnoreCase(serviceInp.getServiceId())){
                    return true;
                }
            }
        }
        return false;
    }

    public static List<Service> valideServiceId(List<Service> allService){
        List<Service> serviceInputTemp = new ArrayList<>();

        while (true) {
            System.out.print("\nSilahkan Masukan Service Id : ");
            String serviceId = input.nextLine();
            
            Service serviceTemp = SalonService.getServiceById(serviceId, allService);

            if (serviceTemp != null){

                if(!(isServiceAlreadyInputed(serviceInputTemp, serviceTemp))){
                    serviceInputTemp.add(serviceTemp);

                    if (isServiceInputHaveSameWithRepo(allService, serviceInputTemp)){
                        return serviceInputTemp;
                    }

                    String isAnotherInputService = validateInput("Ingin pilih service yang lain (Y/T) ?", "^(y|Y|t|T)$", "Input tidak sesuai");
                
                    if (isAnotherInputService.equalsIgnoreCase("t")){
                        return serviceInputTemp;
                    }

                } else{
                    System.out.println("Service sudah dipilih");
                }

            } else{
                System.out.println("Service yang dicari tidak tersedia");
            } 
        }
    }

    public static boolean isServiceInputHaveSameWithRepo(List<Service> service1, List<Service> service2){
       boolean result = false;

       if (service1.size() == service2.size()){
            result = true;
       }

       return result;
    }

    public static String validateReservationId(List<Reservation> allReservation){
        while (true) {
            System.out.print("\nSilahkan Masukan Reservation Id : ");
            String reservationId = input.nextLine();

            Reservation reservationTemp = ReservationService.getReservationByReservationId(allReservation, reservationId);

            if (reservationTemp == null){
                System.out.println("Reservation yang dicari tidak tersedia");

            } else{
                if (reservationTemp.getWorkstage().equalsIgnoreCase("in process")){
                    return reservationId;

                } else {
                    System.out.println("Reservation yang dicari sudah selesai");
                }
            }
        }
    }
}
